﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public float distanceToWalk;
    public float velocity;
    public float pursuitVelocity;

    public float distanceToPlayer;
    public float distanceTrigger;

    public bool pursuitingMode;
    public bool walkingMode;

    public PlayerMovement player;

    public Rigidbody2D enemyRB;

    public GameObject visuals; 

	void Start () {
        player = FindObjectOfType<PlayerMovement>();
        enemyRB = GetComponent<Rigidbody2D>();
        visuals = GameObject.Find("EnemyGraphics");
    }
	
	// Update is called once per frame
	void Update () {

        if (enemyRB.velocity.x > 0.1f)
            visuals.transform.eulerAngles = new Vector2(0, 0);
        else visuals.transform.eulerAngles = new Vector2(0, 180);


        if (!pursuitingMode)
        {
            WalkMode();
            distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
            if (distanceToPlayer <= distanceTrigger)
            {
                pursuitingMode = true;
                walkingMode = false;
            }
        }

        else PursuitMode();

        if (gameObject.GetComponent<Health>().health <= 0)
        {
            Debug.Log("Ha muerto");
            Destroy(gameObject);
        }
    }

    void WalkMode()
    {
        walkingMode = true;

        enemyRB.velocity = new Vector2(Mathf.Sin(Time.time * velocity) * distanceToWalk, enemyRB.velocity.y);
    }

    void PursuitMode()
    {

        pursuitingMode = true;
        walkingMode = false;

        //Si el enemigo esta mas hacia la izquierda que el jugador, este se mueve hacia al derecha; y si no, al contrario

        if (transform.position.x < player.transform.position.x)
            enemyRB.velocity = new Vector2(pursuitVelocity, enemyRB.velocity.y);
       
        else enemyRB.velocity = new Vector2(-pursuitVelocity, enemyRB.velocity.y);

        


        //Debug.Log("Que pacha crackkk");
    }
}




