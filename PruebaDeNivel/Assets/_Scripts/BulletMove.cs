﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    public Rigidbody2D bulletRB;
    public float speed = 10f;
    public float lifeTime = 1f;

    void Start()
    {
        bulletRB = GetComponent<Rigidbody2D>();
    }

    void Update ()
    {

        bulletRB.velocity = transform.right * speed;

        Destroy(gameObject, lifeTime);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.gameObject.GetComponent<Health>() != null)
        { 
            collision.gameObject.GetComponent<Health>().health--;
            Destroy(gameObject);
        }
    }
}
