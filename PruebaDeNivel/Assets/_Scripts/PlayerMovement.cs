﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Animator playerController;

    public GameObject visuals;

    public KeyCode bendKC;
    public bool isBent = false;
    public bool canStand;
    public Collider2D bentCollider;
    public Collider2D standCollider;

    public float maxSpeed;
    public float speed;

    public bool inGround = true;
    public bool doubleJumpActivated = false;

    public KeyCode jump = KeyCode.Space;

    public float distance;
    public float jumpForce;
    public float doubleJumpForce;
    public float gravityUp;
    public float gravityDown; 

    public bool isLookingRight = true;

    public bool isDead = false;

    public Rigidbody2D  bodyRB;


    //Aqui se implementa el movimiento en horizontal del personaje y la direccion hacia la que mira
    void DoForwardMovement()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f)
        {
            speed = Input.GetAxis("Horizontal") * maxSpeed;
            
                if (speed < 0 && isLookingRight)
                    visuals.transform.eulerAngles = new Vector2(0, 180);

                else if (speed > 0 && !isLookingRight)
                    visuals.transform.eulerAngles = new Vector2(0, 0);


            if (speed > maxSpeed)
                speed = maxSpeed;
            if (speed < -maxSpeed)
                speed = -maxSpeed;

            bodyRB.velocity = new Vector2(speed * Time.deltaTime, bodyRB.velocity.y);
        }

        else
        {
            speed = 0f;
            bodyRB.velocity = new Vector2(speed * Time.deltaTime, bodyRB.velocity.y);
        }

        if (speed > 0)
            isLookingRight = true;
        else if (speed < 0)
            isLookingRight = false;

        playerController.SetFloat("player_Walk", Mathf.Abs(bodyRB.velocity.x));
    }


    //Aqui se implementa el salto y el doble salto, comprobando para ello si esta en el suelo o si aun puede hacer el soble salto
    void DoJump()
    {

        inGround =
            Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), -Vector2.up, distance) ||
            Physics2D.Raycast(new Vector2(transform.position.x + 0.5f, transform.position.y), -Vector2.up, distance) || 
            Physics2D.Raycast(new Vector2(transform.position.x - 0.5f, transform.position.y), -Vector2.up, distance);

        Debug.DrawLine(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x, transform.position.y - distance), Color.blue);
        Debug.DrawLine(new Vector2(transform.position.x + 0.5f, transform.position.y), new Vector2(transform.position.x + 0.5f, transform.position.y - distance), Color.blue);
        Debug.DrawLine(new Vector2(transform.position.x - 0.5f, transform.position.y), new Vector2(transform.position.x - 0.5f, transform.position.y - distance), Color.blue);

        if (inGround || doubleJumpActivated)
        {
            if (inGround)
            {
                if (Input.GetKeyDown(jump))
                {
                    bodyRB.velocity = new Vector2(bodyRB.velocity.x, jumpForce);
                    doubleJumpActivated = true;
                }
            }

            else if (doubleJumpActivated)
            {
                if (Input.GetKeyDown(jump))
                {
                    bodyRB.velocity = new Vector2(bodyRB.velocity.x, doubleJumpForce);
                    doubleJumpActivated = false;
                }
            }            
        }

        playerController.SetBool("inGround", inGround);
    }


    //Aqui se implementa la gravedad que afectara al personaje SOLO cuando esta saltando
    void Gravity()
    {
        if (!inGround)
        {
            if (bodyRB.velocity.y > 0)
                bodyRB.velocity += -Vector2.up * Time.deltaTime * gravityUp;
            else bodyRB.velocity += -Vector2.up * Time.deltaTime * gravityDown;
        }
    }

    //Aqui se implementa el movimiento de agacharse. Solo se levantara cuando sea posible, es decir, cuando no haya ningun obstaculo encima del personaje
    void DoBendMovement()
    {
        if (isBent)
        {
            canStand =
             !Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y + 2.1f), Vector2.up, distance) &&
             !Physics2D.Raycast(new Vector2(transform.position.x + 0.5f, transform.position.y + 2.1f), Vector2.up, distance) &&
             !Physics2D.Raycast(new Vector2(transform.position.x - 0.5f, transform.position.y + 2.1f), Vector2.up, distance);

            Debug.DrawLine(new Vector2(transform.position.x, transform.position.y + 2.1f), new Vector2(transform.position.x, transform.position.y + 2.2f), Color.red);
            Debug.DrawLine(new Vector2(transform.position.x + 0.5f, transform.position.y + 2.1f), new Vector2(transform.position.x + 0.5f, transform.position.y + 2.2f), Color.red);
            Debug.DrawLine(new Vector2(transform.position.x - 0.5f, transform.position.y + 2.1f), new Vector2(transform.position.x - 0.5f, transform.position.y + 2.2f), Color.red);

            maxSpeed = 150f;
        }

        else maxSpeed = 300f;

        if (inGround)
        {
            if (Input.GetKey(bendKC))
            {
                isBent = true;
                standCollider.enabled = false;
                bentCollider.enabled = true;
            }
            else if (canStand)
            {
                isBent = false;
                bentCollider.enabled = false;
                standCollider.enabled = true;
            }
        }
         
        playerController.SetBool("Bent", isBent);
    }

	void Start () {

        bodyRB = GetComponent<Rigidbody2D>();
        visuals = GameObject.Find("playerGraphics");
	}
	
    IEnumerator DeathCR()
    {
        playerController.SetBool("Dead", isDead);

        yield return new WaitForSeconds(1f);

        Application.LoadLevel(Application.loadedLevel);
        isDead = false;
        playerController.SetBool("Dead", isDead);

    }

	void Update () {

        if(!isDead)
        {
            DoForwardMovement();
            DoJump();
            Gravity();
            DoBendMovement();
        }

        if (gameObject.GetComponent<Health>().health <= 0)
        {
            isDead = true;
            StartCoroutine(DeathCR());
        }
    }
}
