﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement02 : MonoBehaviour {

    public float distanceToWalk;
    public float velocity;

    public float distanceToPlayer;
    public float distanceTrigger;

    public bool shotingMode;
    public bool walkingMode;
    public bool canShot;
    public float coolDown;

    public GameObject bullet;

    public PlayerMovement player;
    public Rigidbody2D enemyRB;
    public GameObject visuals;
    public GameObject firePoint;

    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerMovement>();
        enemyRB = GetComponent<Rigidbody2D>();
        visuals = GameObject.Find("EnemyGraphics");
        firePoint = GameObject.Find("EnemyGraphics/FirePoint");
	}
	
	// Update is called once per frame
	void Update () {

        if (enemyRB.velocity.x > 0.1f)
            visuals.transform.eulerAngles = new Vector2(0, 0);
        else visuals.transform.eulerAngles = new Vector2(0, 180);

        if (!shotingMode)
        {
            WalkMode();
            distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
            if (distanceToPlayer <= distanceTrigger)
            {
                shotingMode = true;
                walkingMode = false;
            }
        }

        else ShotMode();

        if (gameObject.GetComponent<Health>().health <= 0)
        {
            Debug.Log("Ha muerto");
            Destroy(gameObject);
        }
    }

    void WalkMode()
    {
        walkingMode = true;
        enemyRB.velocity = new Vector2(Mathf.Sin(Time.time * velocity) * distanceToWalk, enemyRB.velocity.y);
    }

    void ShotMode()
    {
        enemyRB.velocity = new Vector2(0, 0);
        if (transform.position.x < player.transform.position.x)
            visuals.transform.eulerAngles = new Vector2(0, 0);
        else visuals.transform.eulerAngles = new Vector2(0, 180);

        Shooting();
    }

    void Shooting()
    {
        if (canShot)
        {
            StartCoroutine(ShotCR());
        }
    }

    IEnumerator ShotCR()
    {
        canShot = false;
        Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation);

        yield return new WaitForSeconds(coolDown);
        canShot = true;
    }
}
