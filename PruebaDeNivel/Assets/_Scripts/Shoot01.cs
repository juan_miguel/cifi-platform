﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot01 : MonoBehaviour {

    public KeyCode shootKC = KeyCode.Q;
    public Transform firePoint;
    public GameObject bullet;
    public float coolDown;
    public bool canShot = true;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Shot();
	}

    void Shot()
    {
        if (Input.GetKey(shootKC))
        {
            if (canShot)
                StartCoroutine(ShotCR());
        }
    }

    IEnumerator ShotCR()
    {
        Debug.Log("yipea");
        canShot = false;
        Instantiate(bullet, firePoint.position, firePoint.rotation);

        yield return new WaitForSeconds(coolDown);
        canShot = true;
    }
}
